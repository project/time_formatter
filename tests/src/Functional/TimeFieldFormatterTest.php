<?php

declare(strict_types=1);

namespace Drupal\Tests\time_formatter\Functional;

use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Tests\BrowserTestBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use PHPUnit\Framework\Attributes\Group;

/**
 * Test time_formatter functionality.
 */
#[Group('time_formatter')]
final class TimeFieldFormatterTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'time_formatter',
    'field',
    'node',
  ];

  /**
   * Test callback.
   */
  public function testTimeFormatter(): void {
    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'access content overview',
      'administer nodes',
      'bypass node access',
    ]);
    $this->drupalLogin($admin_user);

    // Create a test content type.
    $this->drupalCreateContentType(['type' => 'page']);

    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_decimal_field',
      'entity_type' => 'node',
      'type' => 'decimal',
      'settings' => ['precision' => 8, 'scale' => 4],
    ]);
    $fieldStorage->save();
    $fieldConfig = FieldConfig::create([
      'field_name' => $fieldStorage->getName(),
      'field_storage' => $fieldStorage,
      'entity_type' => 'node',
      'bundle' => 'page',
      'label' => $this->randomString(),
    ]);
    $fieldConfig->save();

    // Assign widget settings for the default form mode.
    $settings = [
      'region' => 'content',
      'type' => 'number',
      'settings' => [
        'placeholder' => '0.00',
      ],
      'third_party_settings' => [],
    ];

    $display_repository = \Drupal::service('entity_display.repository');
    assert($display_repository instanceof EntityDisplayRepository);
    $display_repository->getFormDisplay('node', 'page')
      ->setComponent($fieldStorage->getName(), ['type' => 'number'] + $settings)
      ->save();

    $this->drupalGet('/node/add/page');
    $this->submitForm([
      'Title' => $this->randomString(),
      'field_decimal_field[0][value]' => '1234.5678',
    ], 'Save');
    $assert = $this->assertSession();

    $display_repository->getViewDisplay('node', 'page')
      ->setComponent($fieldStorage->getName(), ['type' => 'number_time'])
      ->save();
    $this->drupalGet('node/1');
    $assert->pageTextContains($fieldConfig->getLabel() . ' 0:00:01.235');
  }

}
